Team Freezerburn's Switch A Room

     About:
A game where a child tries to find the missing pieces of his treasured board game in an ever shifting house of adventure.
(Note, the web version doesn't seem to work the first time loaded, refresh and it should be fine)

     Team Members:
Peter McNamara - Lead Designer, Programmer
Ashley Ervin - Lead Artist, Programmer
Matt Fobare - Programmer, Artist
John Fediaczko - Programmer, Artist, Debugger

http://globalgamejam.org/2015/games/switch-room