﻿using UnityEngine;
using System.Collections;

public class ParticleEffect : MonoBehaviour {

    private int counter = 0;

	// Update is called once per frame
	void Update ()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, -5);
        counter += 1;
        if (counter > 60)
        {
            Debug.Log(counter);
            Destroy(this.gameObject);
        }
	}
}
