﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterMovement : MonoBehaviour {

    enum Direction { up, down, left, right };
	enum PreviousKey{ up, down, left, right};
    
	Direction dir;
	PreviousKey pKey;

    public float speed;

	private Animator animator;

    //private CameraController camera = Camera.main.GetComponent<CameraController>();

	// Use this for initialization
	void Start () {
        dir = Direction.down;
		pKey = PreviousKey.left;
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {        
        MoveCharacter();        
	}

    public void MoveCharacter()
    {
        //Block 1
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            dir = Direction.up;
            pKey = PreviousKey.up;
        }
        else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            dir = Direction.right;
            pKey = PreviousKey.right;
        }
        else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            dir = Direction.left;
            pKey = PreviousKey.left;
        }
        else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            dir = Direction.down;
            pKey = PreviousKey.down;
        }

        //Block2 - sets movement and 
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            transform.position += speed * new Vector3(0, 1, 0);
            animator.SetBool("UpPress", true);
            animator.SetBool("DownPress", false);
            animator.SetBool("LeftPress", false);
            animator.SetBool("RightPress", false);
        }
        else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            transform.position += speed * new Vector3(1, 0, 0);
            animator.SetBool("RightPress", true);
            animator.SetBool("DownPress", false);
            animator.SetBool("LeftPress", false);
            animator.SetBool("UpPress", false);
        }
        else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            transform.position -= speed * new Vector3(1, 0, 0);
            animator.SetBool("LeftPress", true);
            animator.SetBool("DownPress", false);
            animator.SetBool("RightPress", false);
            animator.SetBool("UpPress", false);
        }
        else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            transform.position -= speed * new Vector3(0, 1, 0);
            animator.SetBool("DownPress", true);
            animator.SetBool("LeftPress", false);
            animator.SetBool("RightPress", false);
            animator.SetBool("UpPress", false);
        }
        //Block 3 - sets idle 
        if (Input.anyKey == false && pKey == PreviousKey.down)
        {
            animator.SetBool("DownPress", false);
        }

        else if (Input.anyKey == false && pKey == PreviousKey.left)
        {
            animator.SetBool("LeftPress", false);
        }

        else if (Input.anyKey == false && pKey == PreviousKey.right)
        {
            animator.SetBool("RightPress", false);
        }

        else if (Input.anyKey == false && pKey == PreviousKey.up)
        {
            animator.SetBool("UpPress", false);
        }   
    }
}
