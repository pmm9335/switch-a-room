﻿using UnityEngine;
using System.Collections;

public class ObjThatMovesWithObjectives : MonoBehaviour {

    public GameObject plyr;
    public int ObjectiveToWatch;
    public Vector2 position1;
    public Vector2 position2;
    public Vector2 position3;
    public Vector2 position4;
    public Vector2 position5;
    public Vector2 position6;
    

	// UpdateObjs is called by the player during room changes
	public void UpdateObjs()
    {
        GameObject[] tempArray = GameObject.FindGameObjectsWithTag("Player");
        plyr = tempArray[0];

        //Look at what objective to watch
        switch (ObjectiveToWatch)
        {
            case 0:
                {
                    //Look at the progress on that objective
                    switch (plyr.gameObject.GetComponent<GameObjective>().obj1)
                    {
                        case GameObjective.Objective1.start:
                            {
                                transform.position = new Vector3(position1.x,position1.y,transform.position.z);
                                Debug.Log("Stuff happened (start)");
                                break;
                            }
                        case GameObjective.Objective1.part1:
                            {
                                transform.position = new Vector3(position2.x, position2.y, transform.position.z);
                                Debug.Log("Stuff happened (1)");
                                break;
                            }
                        case GameObjective.Objective1.part2:
                            {
                                transform.position = new Vector3(position3.x, position3.y, transform.position.z);
                                Debug.Log("Stuff happened (2)");
                                break;
                            }
                        case GameObjective.Objective1.part3:
                            {
                                transform.position = new Vector3(position4.x, position4.y, transform.position.z);
                                Debug.Log("Stuff happened (3)");
                                break;
                            }
                        case GameObjective.Objective1.part4:
                            {
                                transform.position = new Vector3(position5.x, position5.y, transform.position.z);
                                Debug.Log("Stuff happened (4)");
                                break;
                            }
                        case GameObjective.Objective1.part5:
                            {
                                transform.position = new Vector3(position6.x, position6.y, transform.position.z);
                                Debug.Log("Stuff happened (5)");
                                Application.LoadLevel("endScreen");
                                break;
                            }
                        default:
                            {
                                Debug.Log("Default");
                                break;
                            }
                    }

                    break;
                }
            case 1:
                {
                    //Look at the progress on that objective
                    switch (plyr.gameObject.GetComponent<GameObjective>().obj2)
                    {
                        case GameObjective.Objective2.start:
                            {
                                transform.position = position1;
                                break;
                            }
                        case GameObjective.Objective2.part1:
                            {
                                transform.position = position2;
                                break;
                            }
                        case GameObjective.Objective2.part2:
                            {
                                transform.position = position3;
                                break;
                            }
                        case GameObjective.Objective2.part3:
                            {
                                transform.position = position4;
                                break;
                            }
                        default:
                            break;
                    }
                    break;
                }
            case 2:
                {
                    //Look at the progress on that objective
                    switch (plyr.gameObject.GetComponent<GameObjective>().obj3)
                    {
                        case GameObjective.Objective3.start:
                            {
                                transform.position = position1;
                                break;
                            }
                        case GameObjective.Objective3.part1:
                            {
                                transform.position = position2;
                                break;
                            }
                        case GameObjective.Objective3.part2:
                            {
                                transform.position = position3;
                                break;
                            }
                        default:
                            break;
                    }
                    break;
                }
            default:
                break;
        }
	}
}
