﻿using UnityEngine;
using System.Collections;

public class MainRoomController : MonoBehaviour {

    
    [System.NonSerialized]
    public GameObject currentRoom;
    public float roomEntranceDistanceOffset;
    public GameObject particle;

    void Update()
    {
        //Debug.Log("Transform position: " + transform.position);
        //Debug.Log("RigidBody position: " + rigidbody2D.position);

        //Debug.Log(": " + );
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        switch (other.tag)
        {
            case "Room":
                {
                    //set the room the player is colliding with to the current room.
                    currentRoom = other.gameObject;
                    Debug.Log("The player entered a room");

                    //change camera target, rest is handled by the camera control script
                    Camera.main.GetComponent<CameraController>().targetPos = other.rigidbody2D.position;
                    break;
                }
            case "Collectable":
                {
                    Debug.Log("Touched a collectable");
                    other.GetComponent<CollidableCollectable>().moveObjective(this.gameObject);

                    GameObject[] tempArray = GameObject.FindGameObjectsWithTag("Collectable");

                    foreach (GameObject duderMannBrosef in tempArray)
                    {
                        Instantiate(particle, other.transform.position, Quaternion.identity);
                        duderMannBrosef.gameObject.GetComponent<ObjThatMovesWithObjectives>().UpdateObjs();
                    }
                    break;
                }
            default:
                {
                    Debug.Log("Touched a nonRoom, nonCollectable object trigger");
                    break;
                }
        }
    }

    //Replace references to transform.position to references to rigidbody.position
    //Add a camera manipulating event for if the side is true
    void OnTriggerExit2D(Collider2D other)
    {
        //if the trigger isn't a room, ignore and exit the event
        if (other.CompareTag("Room")==false)
        {
            Debug.Log("Left a nonRoom object trigger");
            return;
        }
        Debug.Log("The player exited a room");
        if(gameObject.transform.position.x > currentRoom.transform.position.x + currentRoom.GetComponent<RoomAttributes>().xOffset)
        {
            if(currentRoom.GetComponent<RoomAttributes>().right==true)
            {
                transform.position += new Vector3(roomEntranceDistanceOffset, 0, 0);
            }
            else
            {
                transform.position += .45f * new Vector3(-1, 0, 0);
                //plyr.position.x -= plyr.GetComponent<CharacterMovement>().speed;
            }
        }
        else if (gameObject.transform.position.x < currentRoom.transform.position.x - currentRoom.GetComponent<RoomAttributes>().xOffset)
        {
            if (currentRoom.GetComponent<RoomAttributes>().left == true)
            {
                transform.position += new Vector3(-roomEntranceDistanceOffset, 0, 0);
            }
            else
            {
                transform.position += .45f * new Vector3(1, 0, 0);
            }
        }
        else if (gameObject.transform.position.y < currentRoom.transform.position.y + 0)
        {
            if (currentRoom.GetComponent<RoomAttributes>().down == true)
            {
                transform.position += new Vector3(0, 2*-roomEntranceDistanceOffset, 0);
            }
            else
            {
                transform.position += .45f * new Vector3(0, 1, 0);
            }
        }
        else if (gameObject.transform.position.y > currentRoom.transform.position.y + 0)
        {
            if (currentRoom.GetComponent<RoomAttributes>().up == true)
            {
                transform.position += new Vector3(0, 2*roomEntranceDistanceOffset, 0);
            }
            else
            {
                transform.position += .45f * new Vector3(0, -1, 0);
            }
        }
    }
}
