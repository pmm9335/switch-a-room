﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameObjective : MonoBehaviour 
{
    public enum Objective1 { start, part1, part2, part3, part4, part5 };
    //start: player in room
    //part1: when player leaves PLAYERS_ROOM                                               (Change things in PLAYERS_ROOM)
    //part2: when plyaer talks to MOM in KITCHEN put OBJECTIVE_1 in PLAYERS_ROOM           (Change things in PLAYERS_ROOM)

    public enum Objective2 { start, part1, part2, part3 };
    //start: player in room
    //part1: when player enters BROTHERS_ROOM put BROTHER in BASEMENT                      (Change things in BASEMENT)
    //part2: when player interacts with BROTHER leaves room and goes to DINING_ROOM        (Change things in DINING_ROOM)
    //part3: when player interacts with BROTHER again put OBJECTIVE_2 in BASEMENT          (Change things in BASEMENT)

    public enum Objective3 { start, part1, part2 };
    //start: Player in room
    //part1: when player interacts with DAD                                                 (Change things in KITCHEN)
    //part2: when player enters KITCHEN DAD has new dialog availible
    //part3: when player interacts with dad put NEWSPAPER in BATHROOM_2                     (Change things in BATHROOM_2)
    //part4: when player gives DAD newspaper put OBJECTIVE_3 in ATTIC                       (Change things in ATTIC)

	// Use this for initialization

    [System.NonSerialized]
	public Objective1 obj1;
    [System.NonSerialized]
	public Objective2 obj2;
    [System.NonSerialized]
	public Objective3 obj3;

	void Start () 
	{
        obj1 = Objective1.start;
        obj2 = Objective2.start;
        obj3 = Objective3.start;
	}
	
	// Update is called once per frame
	void Update () 
	{
        //Debug.Log("Obj1: " + obj1 + "\nObj2: " + obj2);
        //Debug.Log("\nObj3: " + obj3);
	}
}
