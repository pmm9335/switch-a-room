﻿using UnityEngine;
using System.Collections;

public class StartScreen : MonoBehaviour {

    public string targetScene;

	public void OnGUI()
    {
        if (Event.current.type == EventType.KeyDown) { KeyPressedEventHandler(); }
        if (Event.current.type == EventType.MouseDown) { KeyPressedEventHandler(); }

    }

    private void KeyPressedEventHandler() { Application.LoadLevel(targetScene); }
}
