﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    private bool isCameraMoving = false;
    public float scrollSpeed;
    [System.NonSerialized]
    public Vector2 targetPos;

    public bool IsCameraMoving
    {
        get { return isCameraMoving; }
    }

    void Update()
    {
        Vector2 tempVec = new Vector2(transform.position.x,transform.position.y);

        if (targetPos != tempVec)
        {
            ToTarget(targetPos);
        }
    }

    public void ToTarget(Vector3 target)
    {        
        Vector3 toTargetTemp = target - transform.position;
        Vector3 toTarget = new Vector3(toTargetTemp.x, toTargetTemp.y, 0);

        if (toTarget.magnitude > 1)
        {
            isCameraMoving = true;
            toTarget = toTarget.normalized;
            toTarget *= scrollSpeed;
            transform.position += toTarget;            
        }
        else
        {
            isCameraMoving = false;
            Vector3 targetTemp = new Vector3(target.x, target.y, -10);
            transform.position = targetTemp;
        }
    }
}
