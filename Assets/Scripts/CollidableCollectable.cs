﻿using UnityEngine;
using System.Collections;

public class CollidableCollectable : MonoBehaviour {

    public int objectiveNumToChange;

    /* //Can't use this, because this would imply the player is a trigger. Which it isn't
    void OnTriggerEnter(Collider other)
    {
    }
    */
    public void moveObjective(GameObject player)
    {
        Destroy(this.gameObject);
            switch (objectiveNumToChange)
            {
                case 0:
                    {
                        player.gameObject.GetComponent<GameObjective>().obj1 += 1;
                        break;
                    }
                case 1:
                    {
                        player.gameObject.GetComponent<GameObjective>().obj2 += 1;
                        break;
                    }
                case 2:
                    {
                        player.gameObject.GetComponent<GameObjective>().obj3 += 1;
                        break;
                    }
                default:
                    break;
            }
    }

}
