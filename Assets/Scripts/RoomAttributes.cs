﻿using UnityEngine;
using System.Collections;



public class RoomAttributes : MonoBehaviour
{
    /*
    This is just a note I'm using to keep track of my productivity (Peter McNamara)
    Script started around 11pm on Friday 1/23/2015
    Finished first version at 11:30ish pm on Friday 1/23/2015
    */
    public bool up;
    public bool down;
    public bool left;
    public bool right;

    //These public variables will not be shown in the inspector
    //http://docs.unity3d.com/412/Documentation/ScriptReference/NonSerialized.html
    [System.NonSerialized]
    public float xOffset = 6.5f;
    [System.NonSerialized]
    public float yOffset = 5.5f;

}
