﻿using UnityEngine;
using System.Collections;

public class BoredomMeter : MonoBehaviour 
{

	float maxValue = 100f;
	float minValue = 0f;
	float currentValue;
	int timeCouter;
	float startingPos;

	// Use this for initialization
	void Start () 
	{
		timeCouter = 0;
		currentValue = 100f;
		startingPos = transform.position.x;
	}
	
	// Update is called once per frame
	void Update () 
	{


		if (timeCouter == 5) 
		{
			currentValue-= 0.1f;

			timeCouter = 0;
		}
		else
		{
			timeCouter++;
		}

		if(currentValue > maxValue)
		{
			currentValue = maxValue;
		}

		if (currentValue < minValue)
		{
			currentValue = minValue;
			//Game Over
		}

		Debug.Log("Boredome Meter: "+ currentValue);

		float barWidth = currentValue/100;


		transform.localScale = new Vector3 (barWidth, 1f, 0);

		float barLocationRight = startingPos - (1 - barWidth); 
		transform.position = new Vector3 (barLocationRight, transform.position.y, -1.5f);
	}
}
